from parser import parse_file
from validator import Validator, Error

if __name__ == '__main__':
    filename = 'fsa.txt'
    states, alpha, initials, finals, transitions = parse_file(filename)
    f = open('result.txt', 'w')

    validator = Validator(states, alpha, initials, finals, transitions)

    try:
        validator.validation()

        is_complete = validator.check_for_completeness_and_deterministic()
        if is_complete:
            f.write("FSA is complete")
        else:
            f.write("FSA is incomplete")

        warnings = sorted(validator.get_warnings())

        if warnings:
            f.write("\nWarning:")
            for warning in warnings:
                f.write("\n" + warning)

    except Error as e:
        f.write("Error:\n")
        f.write(str(e))

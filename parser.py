def parse_line(str, allow_underscore=False, allow_grater_and_underscore=False):
    if not allow_underscore and not allow_grater_and_underscore:
        str = set(''.join([i for i in str if i.isalpha() or i.isdigit() or i in [',']]).split(','))
    elif allow_grater_and_underscore:
        str = set(''.join([i for i in str if i.isalpha() or i.isdigit() or i in [',', '_', '>']]).split(','))
    else:
        str = set(''.join([i for i in str if i.isalpha() or i.isdigit() or i in [',', '_']]).split(','))

    str -= {''}  # remove empty string

    if str:
        return str
    else:
        return {}


def parse_file(filename):
    f = open(filename, 'r')

    states = f.readline()[6:]
    states = parse_line(states)

    alpha = f.readline()[5:]
    alpha = parse_line(alpha, True)

    initial = f.readline()[7:]
    initial = parse_line(initial)

    final = f.readline()[6:]
    final = parse_line(final)

    trans = f.readline()[6:]
    trans = parse_line(trans, allow_grater_and_underscore=True)
    new_trans = {}
    for t in trans:
        t = t.split('>')
        if t[0] in new_trans:
            new_trans[t[0]] += [tuple(t[1:])]
        else:
            new_trans[t[0]] = [tuple(t[1:])]

    return states, alpha, initial, final, new_trans

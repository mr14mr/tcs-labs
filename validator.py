import re


class Error(Exception):
    message = ''

    def __str__(self):
        return self.message


class E1(Error):
    def __init__(self, state_name):
        self.message = 'E1: A state \'{0}\' is not in set of states'.format(state_name)
        super().__init__()


class E2(Error):
    def __init__(self):
        super().__init__()
        self.message = 'E2: Some states are disjoint'


class E3(Error):
    def __init__(self, transition_name):
        self.message = 'E3: A transition \'{0}\' is not represented in the alphabet'.format(transition_name)
        super().__init__()


class E4(Error):
    def __init__(self):
        self.message = 'E4: Initial state is not defined'
        super().__init__()


class E5(Error):
    def __init__(self):
        self.message = 'E5: Input file is malformed'
        super().__init__()


class Validator:
    _warnings = []
    _states = {}
    _alpha = {}
    _initials = {}
    _finals = {}
    _transitions = {}

    def __init__(self, states, alpha, initials, finals, transitions):
        self._states = states
        self._alpha = alpha
        self._initials = initials
        self._finals = finals
        self._transitions = transitions

    def get_warnings(self):
        return self._warnings

    def check_for_completeness_and_deterministic(self):
        is_complete = True
        for s in self._states:

            transitions_list = [transition[0] for transition in self._transitions[s]]
            transitions_set = set(transitions_list)

            # if there is some duplications => it is not deterministic FSA
            if len(transitions_list) != len(transitions_set):
                self._warnings += ["W3: FSA is nondeterministic"]

            if transitions_set != self._alpha:
                is_complete = False
                break

        return is_complete

    def _check_for_disjoint(self):
        def _dfs(g, first, discovered=[]):
            visited, to_visit = set(), [first]
            while to_visit:
                vertex = to_visit.pop()

                # this part is used for detecting vertexes which is unreachable from initial
                if vertex in discovered:
                    return visited, True

                if vertex not in visited:
                    visited.add(vertex)
                    to_visit.extend(g[vertex] - visited)
            return visited, False

        graph = {}

        for s in self._states:
            graph[s] = []

        for beginning_state in self._transitions:

            for path in self._transitions[beginning_state]:
                if beginning_state in graph:
                    graph[beginning_state] += [(path[1])]
                else:
                    graph[beginning_state] = [(path[1])]

        for k in graph:
            graph[k] = set(graph[k])
        component = {}

        for initial in self._initials:
            _component, _ = _dfs(graph, initial)
            component = set(list(component) + list(_component))

        if len(self._states) != len(set(component)):
            diff = self._states - component

            for v in diff:
                visited, is_connected = _dfs(graph, v, list(component))
                if not is_connected:
                    raise E2()
                else:
                    self._warnings += ["W2: Some states are not reachable from initial state"]

    def validation(self):

        # check if init not empty
        if not set(self._initials):
            raise E4

        # check if initials not in states
        for initial in self._initials:
            if initial not in self._states:
                raise E1(initial)

        # check if finals not in states
        for final in self._finals:
            # check if finals not in states
            if final not in self._states:
                raise E1(final)

        self._check_for_disjoint()

        # check if all transitions are in alphabet
        for transition_initial in self._transitions:
            l = self._transitions[transition_initial]

            for transition in l:
                if not transition[0] in self._alpha:
                    raise E3(transition[0])

        # if final state is not defined
        if not set(self._finals):
            self._warnings += ["W1: Accepting state is not defined"]

        # this block is here just because of requirements that input validation (!!) should be checked after other
        # errors Which is not <i>good</i>

        f = open('fsa.txt', 'r')
        r = re.compile("states={\S*}\nalpha={\S*}\ninit.st={\S*}\nfin.st={\S*}\ntrans={\S*}")
        if not r.match(f.read()):
            raise E5

